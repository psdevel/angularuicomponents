export  interface ColumnDefinitionInterface {
  field: string;
  title: string;
  dataType: 'string' | 'number' | 'date' | 'boolean' | 'object';
  visible: boolean;
  format?: string;
}
