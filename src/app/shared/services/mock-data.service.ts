import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmployeeDto} from '../dtos/employee.dto';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MockDataService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getMockEmployees(parameters?: HttpParams): Observable<HttpResponse<EmployeeDto>> {
    return this.http.get<any>('http://localhost:3001/employee', {params: parameters, observe: 'response'})
      .pipe(
        map((result) => {
          result.body.forEach((employeeData) => {
            employeeData.last_login = new Date(employeeData.last_login);
          });
          return result;
        })
      );
  }

  putMockEmployees(id: number, employee: EmployeeDto): Observable<HttpResponse<EmployeeDto>> {
    return this.http.put<any>('http://localhost:3001/employee/' + id, employee, {observe: 'response'})
      .pipe(
        map((result) => {
          result.body.last_login = new Date(result.body.last_login);
          return result;
        })
      );
  }

  postMockEmployees(employee: EmployeeDto): Observable<HttpResponse<EmployeeDto>> {
    return this.http.post<any>('http://localhost:3001/employee', employee, {observe: 'response'})
      .pipe(
        map((result) => {
          result.body.last_login = new Date(result.body.last_login);
          return result;
        })
      );
  }

  deleteMockEmployees(id: number): Observable<HttpResponse<EmployeeDto>> {
    return this.http.delete<any>('http://localhost:3001/employee/' + id, {observe: 'response'});
  }

}
