import {CompareOperatorsInterface} from '../interfaces/compare-operators.interface';

export const PAGE_SIZE: number = 30;

/**
 * Inspired by
 * @link https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxFilterBuilder/Field/#filterOperations
 */
export const STRING_COMPARE_OPERATORS: CompareOperatorsInterface[] = [
  {label: 'Contains', icon: '', operator: 'contains'},
  {label: 'Not Contains', icon: '', operator: 'notcontains'},
  {label: 'Equal', icon: '', operator: '='},
  {label: 'Not Equal', icon: '', operator: '<>'},
  {label: 'Starts with', icon: '', operator: 'startswith'},
  {label: 'Ends with', icon: '', operator: 'endswith'},
  // {label: 'Is Blank', icon: '', matchMode: 'isblank'},
  // {label: 'Is Not Blank', icon: '', matchMode: 'isnotblank'}
];
export const NUMERIC_COMPARE_OPERATORS: CompareOperatorsInterface[] = [
  {label: 'Equal', icon: '', operator: '='},
  {label: 'Not Equal', icon: '', operator: '<>'},
  {label: 'Less Than', icon: '', operator: '<'},
  {label: 'Greater Than', icon: '', operator: '>'},
  {label: 'Less Than or Equal To', icon: '', operator: '<='},
  {label: 'Greater Than or Equal To', icon: '', operator: '>='},
  {label: 'Between', icon: '', operator: 'between'},
  // {label: 'Is Blank', icon: '', matchMode: 'isblank'},
  // {label: 'Is Not Blank', icon: '', matchMode: 'isnotblank'}
];
export const DATETIME_COMPARE_OPERATORS: CompareOperatorsInterface[] = [
  {label: 'Equal', icon: '', operator: '='},
  {label: 'Not Equal', icon: '', operator: '<>'},
  {label: 'Less Than', icon: '', operator: '<'},
  {label: 'Greater Than', icon: '', operator: '>'},
  {label: 'Less Than or Equal To', icon: '', operator: '<='},
  {label: 'Greater Than or Equal To', icon: '', operator: '>='},
  {label: 'Between', icon: '', operator: 'between'},
  // {label: 'Is Blank', icon: '', matchMode: 'isblank'},
  // {label: 'Is Not Blank', icon: '', matchMode: 'isnotblank'}
];
export const BOOLEAN_COMPARE_OPERATORS: CompareOperatorsInterface[] = [
  {label: 'All', icon: '', operator: '<>'},
  {label: 'Is True', icon: '', operator: '='},
  {label: 'Is False', icon: '', operator: '='},
  // {label: 'Is Blank', icon: '', matchMode: 'isblank'},
  // {label: 'Is Not Blank', icon: '', matchMode: 'isnotblank'}
];
export const OBJECT_COMPARE_OPERATORS: CompareOperatorsInterface[] = [
  {label: 'Is Blank', icon: '', operator: 'isblank'},
  {label: 'Is Not Blank', icon: '', operator: 'isnotblank'}
];
