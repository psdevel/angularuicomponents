import {ColumnDefinitionInterface} from '../interfaces/column-definition.interface';

export const EmployeeColumnsDefinition: ColumnDefinitionInterface[] = [
  {
    field: 'id',
    title: 'ID',
    dataType: 'number',
    visible: true
  },
  {
    field: 'first_name',
    title: 'First Name',
    dataType: 'string',
    visible: true
  },
  {
    field: 'last_name',
    title: 'Last Name',
    dataType: 'string',
    visible: true
  },
  {
    field: 'email',
    title: 'Email',
    dataType: 'string',
    visible: true
  },
  {
    field: 'gender',
    title: 'Gender',
    dataType: 'string',
    visible: true
  },
  {
    field: 'ip_address',
    title: 'IP Address',
    dataType: 'string',
    visible: true
  },
  {
    field: 'last_login',
    title: 'Last Login',
    dataType: 'date',
    format: 'dd/MM/y',
    visible: true
  },
  {
    field: 'valid',
    title: 'Is Valid',
    dataType: 'boolean',
    visible: true
  },
  {
    field: 'company.name',
    title: 'Company Name',
    dataType: 'string',
    visible: false
  },
  {
    field: 'company.address',
    title: 'Company Address',
    dataType: 'string',
    visible: false
  },
  {
    field: 'company.country',
    title: 'Company Country',
    dataType: 'string',
    visible: false
  },
  {
    field: 'company.phone',
    title: 'Company Phone',
    dataType: 'string',
    visible: false
  },
];
