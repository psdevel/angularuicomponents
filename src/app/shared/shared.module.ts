import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MockDataService} from './services/mock-data.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    MockDataService,
  ]
})
export class SharedModule {
}
