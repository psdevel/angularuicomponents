import {MockDataParamsDto} from '../dtos/mock-data-params.dto';
import {HttpParams} from '@angular/common/http';

export class Utils {

  static prepareMockParams(params?: MockDataParamsDto): HttpParams {
    let getParameters: HttpParams = new HttpParams();
    if (params) {
      getParameters = getParameters
        .set('_limit', params.pageSize ? params.pageSize.toString() : '30')
        .set('_page', params.page ? params.page.toString() : '1')
        .set('_order', params.order ? [...params.order].join(',') : 'asc')
        .set('_sort', params.sort ? [...params.sort].join(',') : null)
        .set('q', params.fulltext ? params.fulltext : '');

      if (params.slice) {
        getParameters = getParameters.set('_start', params.slice.start.toString());
        getParameters = getParameters.set('_end', params.slice.end.toString());
      }

      if (params.filters) {
        params.filters.forEach((filter) => {
          getParameters = getParameters.set([...filter.column].join('.'), filter.value);
        });
      }

      if (params.filtersLike) {
        params.filtersLike.forEach((filter) => {
          getParameters = getParameters.set([...filter.column].join('.') + '_like', filter.query);
        });
      }

      if (params.filtersExclude) {
        params.filtersExclude.forEach((filter) => {
          getParameters = getParameters.set([...filter.column].join('.') + '_ne', filter.value);
        });
      }

      if (params.filtersGraterThen) {
        params.filtersGraterThen.forEach((filter) => {
          getParameters = getParameters.set(filter.column + '_gte', filter.valueFrom);
        });
      }

      if (params.filtersLessThen) {
        params.filtersLessThen.forEach((filter) => {
          getParameters = getParameters.set(filter.column + '_lte', filter.valueTo);
        });
      }
    }
    return getParameters;
  }

  /**
   * Format borrowed from PHP
   * @param date Date
   * @param format string
   * @link https://www.php.net/manual/en/function.date.php#refsect1-function.date-parameters
   */
  static formatDateTime(
    date: Date,
    format: 'Y-m-d' | 'H:i:s' | 'Y-m-dTH:i:s.vZ'
  ): string {
    function pad(num) {
      if (num < 10) {
        return '0' + num;
      }
      return num;
    }

    switch (format) {
      case 'Y-m-d':
        return date.getFullYear() +
          '-' + pad(date.getMonth() + 1) +
          '-' + pad(date.getDate());
      case 'H:i:s':
        return pad(date.getHours()) +
          ':' + pad(date.getMinutes()) +
          ':' + pad(date.getSeconds());
      case 'Y-m-dTH:i:s.vZ':
        return date.getFullYear() +
          '-' + pad(date.getMonth() + 1) +
          '-' + pad(date.getDate()) +
          'T' + pad(date.getHours()) +
          ':' + pad(date.getMinutes()) +
          ':' + pad(date.getSeconds()) +
          '.' + (date.getMilliseconds() / 1000).toFixed(3).slice(2, 5) +
          'Z';
    }
  }

  static lazyLoadStyles(styleNames: string[], document: Document) {
    const head = document.getElementsByTagName('head')[0];
    const lazyLinks = document.querySelectorAll('link[title="lazy-load-styles"]');
    if (lazyLinks.length > 0) {
      lazyLinks.forEach(lazyLink => {
        lazyLink.remove();
      });
    }

    styleNames.forEach(styleName => {
      const style = document.createElement('link');
      style.title = 'lazy-load-styles';
      style.href = styleName;
      style.rel = 'stylesheet';
      head.appendChild(style);
    });
  }

  static filedType2InputType(fieldType: string): string {
    switch (fieldType) {
      case 'string':
        return 'text';
      case 'numeric':
        return 'number';
      case 'boolean':
        return 'checkbox';
      default:
        return fieldType;
    }
  }
}
