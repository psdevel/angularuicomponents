export class MockDataParamsDto {
  page?: number;
  pageSize?: number;
  order?: string[];
  sort?: string[];
  slice?: {start: number, end: number};
  filters?: [{column: string[], value: string}];
  filtersLike?: [{ column: string[], query: string }];
  filtersExclude?: [{ column: string[], value: string }];
  filtersGraterThen?: [{ column: string, valueFrom: string}];
  filtersLessThen?: [{ column: string, valueTo: string}];
  fulltext?: string;
}
