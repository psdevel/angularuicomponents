export class CompanyDto {
  name: string;
  address: string;
  country: string;
  phone: string;
  postal?: string;
}
