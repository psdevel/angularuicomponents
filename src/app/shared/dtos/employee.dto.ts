import {CompanyDto} from './company.dto';

export class EmployeeDto {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  gender: string;
  ipAddress: string;
  company: CompanyDto;
  valid: boolean;
  lastLogin: Date;
}
