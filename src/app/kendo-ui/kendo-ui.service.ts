import {Injectable} from '@angular/core';
import {DataStateChangeEvent} from '@progress/kendo-angular-grid';
import {Utils} from '../shared/utils/utils';
import {CompositeFilterDescriptor, FilterDescriptor, SortDescriptor, State} from '@progress/kendo-data-query';
import {isArray, isDate} from 'util';

@Injectable({
  providedIn: 'root'
})
export class KendoUiService {

  private filterLike: [{ column: string[], query: string }];
  private filterEqual: [{ column: string[], value: string }];
  private filterExclude: [{ column: string[], value: string }];
  private filterGreaterThen: [{ column: string, valueFrom: string }];
  private filterLessThen: [{ column: string, valueTo: string }];

  prepareParams(params: DataStateChangeEvent | State) {
    this.resetFilters();

    params.filter && params.filter.filters ?
      // tslint:disable-next-line:no-unused-expression
      this.parseMultipleFilter(params.filter) : null;

    return Utils.prepareMockParams({
      pageSize: params.take,
      page: (params.skip / params.take) + 1,
      sort: this.getSortedArray(params.sort),
      order: this.getOrderedArray(params.sort),
      filtersLike: this.filterLike,
      filters: this.filterEqual,
      filtersExclude: this.filterExclude,
      filtersGraterThen: this.filterGreaterThen,
      filtersLessThen: this.filterLessThen,
    });
  }

  private parseMultipleFilter(filterDescriptor: CompositeFilterDescriptor|FilterDescriptor) {
    filterDescriptor['filters'].forEach(filter => {
      if (isArray(filter)) {
        this.parseMultipleFilter(filter);
      }
      this.parseFilter(filter);
    });
  }

  // tslint:disable-next-line:ban-types
  private parseFilter(filter: CompositeFilterDescriptor|FilterDescriptor) {
    if (filter) {
      const operator: string = filter['operator'];
      const col: string = filter['field'];
      let val: string = filter['value']; let isValDateTime = false;

      if (isDate(val)) {
        val = Utils.formatDateTime(val as unknown as Date, 'Y-m-dTH:i:s.vZ');
        isValDateTime = true;
      }

      /**
       * Warning !
       * @link https://www.telerik.com/kendo-angular-ui/components/grid/filtering/built-in-template/#toc-setting-the-default-filter-operator
       * @link https://www.telerik.com/kendo-angular-ui/components/dataquery/api/FilterDescriptor/#toc-filterdescriptor
       */
      switch (operator) {
        case 'contains':
          this.filterLike.push({column: [col], query: val});
          break;
        case 'doesnotcontain':
          this.filterLike.push({column: [col], query: '^((?!' + val + ').)*$'});
          break;
        case 'startswith':
          this.filterLike.push({column: [col], query: '^' + val});
          break;
        case 'endswith':
          this.filterLike.push({column: [col], query: val + '$'});
          break;
        case 'eq':
          if (isValDateTime) {
            this.filterLike.push({column: [col], query: '^' + Utils.formatDateTime(new Date(val), 'Y-m-d')});
          } else {
            this.filterEqual.push({column: [col], value: val});
          }
          break;
        case 'neq':
          if (isValDateTime) {
            this.filterLike.push({column: [col], query: '^((?!' + Utils.formatDateTime(new Date(val), 'Y-m-d') + ').)*$'});
          } else {
            this.filterExclude.push({column: [col], value: val});
          }
          break;
        case 'lt':
          if (isValDateTime) {
            const date = new Date(val);
            date.setDate(date.getDate() - 1);
            this.filterLessThen.push({column: col, valueTo: Utils.formatDateTime(date, 'Y-m-d')});
          } else {
          this.filterLessThen.push({column: col, valueTo: (parseInt(val, 10) - 1).toString()});
          }
          break;
        case 'gt':
          if (isValDateTime) {
            const date = new Date(val);
            date.setDate(date.getDate() + 1);
            this.filterGreaterThen.push({column: col, valueFrom: Utils.formatDateTime(date, 'Y-m-d')});
          } else {
          this.filterGreaterThen.push({column: col, valueFrom: (parseInt(val, 10) + 1).toString()});
          }
          break;
        case 'lte':
          this.filterLessThen.push({column: col, valueTo: val});
          break;
        case 'gte':
          this.filterGreaterThen.push({column: col, valueFrom: val});
          break;
        case 'isnotnull':
        case 'isnull':
        case 'isempty':
        case 'isnotempty':
          // TODO: Implementovat isnull
          alert('Testovací server nemá implementovánou možnost \'is null\', \'is not null\', \'is empty\', \'is not empty\'. Prozatimní data nemusí odpovídat zadanému filtru');
          break;
      }
    }
  }
  private getSortedArray(toSort: SortDescriptor[]): string[] {
    const sortArray: string[] = [];
    if (toSort) {
      toSort.forEach(sort => {
        if (sort.dir) {
          sortArray.push(sort.field);
        }
      });
    }
    return sortArray;
  }
  private getOrderedArray(toOrder: SortDescriptor[]): string[] {
    const orderArray: string[] = [];
    if (toOrder) {
      toOrder.forEach(sort => {
        orderArray.push(sort.dir);
      });
    }
    return orderArray;
  }
  private resetFilters() {
    // @ts-ignore
    this.filterExclude = []; this.filterEqual = []; this.filterLike = []; this.filterGreaterThen = []; this.filterLessThen = [];
  }

}
