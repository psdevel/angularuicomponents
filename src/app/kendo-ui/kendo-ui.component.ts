import {Component, Inject, OnInit} from '@angular/core';
import {MockDataService} from '../shared/services/mock-data.service';
import {map, tap} from 'rxjs/operators';
import {DataStateChangeEvent, GridDataResult} from '@progress/kendo-angular-grid';
import {Observable} from 'rxjs';
import {KendoUiService} from './kendo-ui.service';
import {HttpParams} from '@angular/common/http';
import {State} from '@progress/kendo-data-query';
import {PAGE_SIZE} from '../shared/constants/app.constant';
import {EmployeeColumnsDefinition} from '../shared/constants/employee.columns-definition';
import {EmployeeDto} from '../shared/dtos/employee.dto';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Utils} from '../shared/utils/utils';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'app-kendo-ui',
  templateUrl: './kendo-ui.component.html',
  styleUrls: ['./kendo-ui.component.scss']
})
export class KendoUiComponent implements OnInit {

  gridData: Observable<GridDataResult>;
  loading: boolean = false;

  public state: State = {
    skip: 0,
    take: PAGE_SIZE,
    sort: []
  };

  public columns: KendoUIColumnSetting[] = EmployeeColumnsDefinition.map(value => {
    return {
      field: value.field,
      title: value.title,
      type: this.narrowColumnsDefinitionFieldType(value.dataType),
      format: value.format ? value.format : '',
      hidden: !value.visible,
    } as KendoUIColumnSetting;
  });

  private editedRowIndex: number;

  /* Form Driven */
  public formGroup: FormGroup;

  /* Template Driven */
  private editedEmployee: EmployeeDto;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private mockService: MockDataService,
    private kendoUiService: KendoUiService,
  ) {
    Utils.lazyLoadStyles(['kendo-ui-style.css'], this.document);
  }

  ngOnInit() {
    this.loadData();
  }

  dataStateChange(event: DataStateChangeEvent) {
    this.state = event;
    this.loadData();
  }

  public addHandler({sender} /* Template Driven Adding ... , formInstance */) {

    /* Form Driven Adding */
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      gender: new FormControl(''),
      ip_address: new FormControl(''),
      last_login: new FormControl(Utils.formatDateTime(new Date(), 'Y-m-d')),
      valid: new FormControl('', Validators.required)
    });
    sender.addRow(this.formGroup);

    /* Template Driven Adding */
    // sender.addRow(this.formGroup);
    // formInstance.reset();
    // this.closeEditor(sender);
    // sender.addRow(new EmployeeDto());
  }

  /**
   * @param dataItem EmployeeDTO
   */
  public editHandler({sender, rowIndex, dataItem}) {

    /* Form Driven Editing */
    this.closeEditor(sender);
    this.formGroup = new FormGroup({
      first_name: new FormControl(dataItem.firstName),
      last_name: new FormControl(dataItem.lastName),
      email: new FormControl(dataItem.email),
      gender: new FormControl(dataItem.gender),
      ip_address: new FormControl(dataItem.ipAddress),
      last_login: new FormControl(dataItem.lastLogin),
      valid: new FormControl(dataItem.valid)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);

    /* Template Driven Editing */
    // this.closeEditor(sender);
    // this.editedRowIndex = rowIndex;
    // this.editedEmployee = Object.assign({}, dataItem);
    // sender.editRow(rowIndex);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({sender, rowIndex, /* Template Driven Save ...dataItem*/ formGroup, isNew}) {
    /* Form Driven Save */
    const employee: EmployeeDto = formGroup.value;
    this.mockService.postMockEmployees(employee).subscribe(() => {
      sender.closeRow(rowIndex);
    });

    /* Template Driven Save */
    // this.mockService.postMockEmployees(dataItem).subscribe(() => {
    //   sender.closeRow(rowIndex);
    //   this.editedRowIndex = undefined;
    //   this.editedEmployee = undefined;
    // });
  }

  public removeHandler({dataItem}) {
    this.mockService.deleteMockEmployees(dataItem.id).subscribe(() => this.loadData());
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    // this.editService.resetItem(this.editedProduct);
    this.editedRowIndex = undefined;
    this.editedEmployee = undefined;
  }

  private loadData() {
    this.loading = true;
    const params: HttpParams = this.kendoUiService.prepareParams(this.state);
    this.gridData = this.mockService.getMockEmployees(params)
      .pipe(
        map(response => ({
          data: response.body,
          total: response.headers.get('x-total-count')
        } as unknown as GridDataResult)),
        tap(() => this.loading = false)
      );
  }

  private narrowColumnsDefinitionFieldType(fieldType: string): string {
    switch (fieldType) {
      case 'string':
        return 'text';
      case 'number':
        return 'numeric';
      default:
        return fieldType;
    }
  }

  public kendoFiledType2InputType(fieldType: string): string {
    return Utils.filedType2InputType(fieldType);
  }
}

interface KendoUIColumnSetting {
  field: string;
  title: string;
  format?: string;
  type: 'text' | 'numeric' | 'boolean' | 'date';
  hidden: boolean;
}
