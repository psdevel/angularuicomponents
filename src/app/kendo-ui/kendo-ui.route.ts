import {Routes} from '@angular/router';
import {KendoUiComponent} from './kendo-ui.component';

export const kendoUiRoutes: Routes = [
  {
    path: '',
    component: KendoUiComponent,
  }
];
