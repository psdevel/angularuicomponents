import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KendoUiComponent } from './kendo-ui.component';
import {RouterModule} from '@angular/router';
import {kendoUiRoutes} from './kendo-ui.route';
import {BodyModule, ColumnMenuModule, GridModule, SharedModule as SharedKendoModule} from '@progress/kendo-angular-grid';
import {SharedModule} from '../shared/shared.module';
import {KendoUiService} from './kendo-ui.service';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    KendoUiComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(kendoUiRoutes),
    GridModule,
    SharedKendoModule,
    BodyModule,
    FormsModule,
    ColumnMenuModule,
  ],
  providers: [
    KendoUiService,
  ]
})
export class KendoUiModule { }
