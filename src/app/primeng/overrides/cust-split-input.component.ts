import {SplitButton} from 'primeng/splitbutton';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';
import {Table} from 'primeng/table';
import {CompareOperatorsInterface} from '../../shared/interfaces/compare-operators.interface';
import {
  BOOLEAN_COMPARE_OPERATORS,
  DATETIME_COMPARE_OPERATORS,
  NUMERIC_COMPARE_OPERATORS,
  OBJECT_COMPARE_OPERATORS,
  STRING_COMPARE_OPERATORS
} from '../../shared/constants/app.constant';
import {Utils} from '../../shared/utils/utils';

@Component({
  selector: 'cust-p-splitInput',
  templateUrl: './cust-split-input.component.html',
  animations: [
    trigger('overlayAnimation', [
      state('void', style({
        transform: 'translateY(5%)',
        opacity: 0
      })),
      state('visible', style({
        transform: 'translateY(0)',
        opacity: 1
      })),
      transition('void => visible', animate('{{showTransitionParams}}')),
      transition('visible => void', animate('{{hideTransitionParams}}'))
    ])
  ],
  styles: [`
      .active {background-color: #007ad9!important;}
      .active .ui-menuitem-text {color: #ffffff!important;}
  `]
})
export class CustSplitInputComponent extends SplitButton implements OnInit {
  @Input() dropdownIcon = 'pi pi-chevron-down';
  @Input() table: Table;
  @Input() field: string;
  @Input() fieldType: string;
  public matchMode: CompareOperatorsInterface;
  public selectedValue1: any;
  public selectedValue2: any;

  ngOnInit() {
    switch (this.fieldType) {
      case 'date':
        this.model = DATETIME_COMPARE_OPERATORS;
        this.matchMode = DATETIME_COMPARE_OPERATORS[0];
        break;
      case 'object':
        this.model = OBJECT_COMPARE_OPERATORS;
        this.matchMode = OBJECT_COMPARE_OPERATORS[0];
        break;
      case 'boolean':
        this.model = BOOLEAN_COMPARE_OPERATORS;
        this.matchMode = BOOLEAN_COMPARE_OPERATORS[0];
        break;
      case 'number':
        this.model = NUMERIC_COMPARE_OPERATORS;
        this.matchMode = NUMERIC_COMPARE_OPERATORS[0];
        break;
      case 'string':
      default:
        this.model = STRING_COMPARE_OPERATORS;
        this.matchMode = STRING_COMPARE_OPERATORS[0];
        break;
    }
  }

  itemClick(event, item) {
    this.matchMode = item;
    this.triggerFiltering();
  }

  triggerFiltering() {
    // Boolean
    if (this.fieldType === 'boolean') {
      switch (this.matchMode.label) {
        case 'All':
          this.selectedValue1 = null;
          break;
        case 'Is False':
          this.selectedValue1 = 'false';
          break;
        case 'Is True':
          this.selectedValue1 = 'true';
          break;
      }
    }
    // Create date
    if (this.fieldType === 'date') {
      this.selectedValue1 = this.selectedValue1 ? new Date(this.selectedValue1) : null;
      this.selectedValue2 = this.selectedValue2 ? new Date(this.selectedValue2) : null;
    }
    if (this.matchMode.operator === 'between') {
      if (this.selectedValue1 && this.selectedValue2) {
        this.table.filter([this.selectedValue1, this.selectedValue2], this.field, this.matchMode);
      }
    } else {
      this.table.filter(this.selectedValue1, this.field, this.matchMode);
    }
    // Reformat date
    if (this.fieldType === 'date') {
      this.selectedValue1 = this.selectedValue1 ? Utils.formatDateTime(this.selectedValue1, 'Y-m-d') : undefined;
      this.selectedValue2 = this.selectedValue2 ? Utils.formatDateTime(this.selectedValue2, 'Y-m-d') : undefined;
    }
  }

  fieldType2InputType(fieldType: string) {
    return Utils.filedType2InputType(fieldType);
  }

}
