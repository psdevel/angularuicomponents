import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimengComponent } from './primeng.component';
import {RouterModule} from '@angular/router';
import {primengRoutes} from './primeng.route';
import {SharedModule} from '../shared/shared.module';
import {TableModule} from 'primeng/table';
import {PrimengService} from './primeng.service';
import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import {SplitButtonModule} from 'primeng/splitbutton';
import {CustSplitInputComponent} from './overrides/cust-split-input.component';
import {CheckboxModule} from 'primeng/checkbox';
import {AddRowDirective} from './overrides/add-row.directive';

@NgModule({
  declarations: [
    PrimengComponent,
    CustSplitInputComponent,
    AddRowDirective,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(primengRoutes),
    TableModule,
    InputTextModule,
    FormsModule,
    SplitButtonModule,
    CheckboxModule,
  ],
  providers: [
    PrimengService,
  ]
})
export class PrimengModule { }
