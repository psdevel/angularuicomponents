import {Injectable} from '@angular/core';
import {Utils} from '../shared/utils/utils';
import {isArray, isDate, isObject, isString} from 'util';
import {FilterMetadata, LazyLoadEvent, SortMeta} from 'primeng/api';
import {CompareOperatorsInterface} from '../shared/interfaces/compare-operators.interface';

@Injectable({
  providedIn: 'root'
})
export class PrimengService {

  private filterLike: [{ column: string[], query: string }];
  private filterEqual: [{ column: string[], value: string }];
  private filterExclude: [{ column: string[], value: string }];
  private filterGreaterThen: [{ column: string, valueFrom: string }];
  private filterLessThen: [{ column: string, valueTo: string }];

  prepareParams(params: LazyLoadEvent) {
    this.resetFilters();

    if (isObject(params.filters) && Object.keys(params.filters)) {
      this.parseMultipleFilter(params.filters);
    }

    return Utils.prepareMockParams({
      pageSize: params.rows,
      page: (params.first / params.rows) + 1,
      sort: this.getSortedArray(params.multiSortMeta),
      order: this.getOrderedArray(params.multiSortMeta),
      filtersLike: this.filterLike,
      filters: this.filterEqual,
      filtersExclude: this.filterExclude,
      filtersGraterThen: this.filterGreaterThen,
      filtersLessThen: this.filterLessThen,
    });
  }

  private parseMultipleFilter(filters) {
    Object.keys(filters).forEach(keyName => {
      if (isArray(filters[keyName].value) && (filters[keyName].matchMode as unknown as CompareOperatorsInterface).operator === 'between') {
        filters[keyName].value.forEach((val, index) => {
          const newFilter: FilterMetadata = {value: val, matchMode: index === 0 ? '>=' : '<='};
          this.parseFilter(newFilter, keyName);
        });
      } else {
        this.parseFilter(filters[keyName], keyName);
      }
    });
  }

  // tslint:disable-next-line:ban-types
  private parseFilter(filter: FilterMetadata, fieldName: string) {
    if (filter) {
      const operator: string = isString(filter.matchMode) ?
        filter.matchMode :
        (filter.matchMode as unknown as CompareOperatorsInterface).operator;
      const col: string = fieldName;
      let val: string = filter.value; let isValDateTime = false;

      if (isDate(val)) {
        val = Utils.formatDateTime(val as unknown as Date, 'Y-m-d');
        isValDateTime = true;
      }

      /**
       * Warning !
       * @link https://www.telerik.com/kendo-angular-ui/components/grid/filtering/built-in-template/#toc-setting-the-default-filter-operator
       * @link https://www.telerik.com/kendo-angular-ui/components/dataquery/api/FilterDescriptor/#toc-filterdescriptor
       */
      switch (operator) {
        case 'contains':
          this.filterLike.push({column: [col], query: val});
          break;
        case 'doesnotcontain':
          this.filterLike.push({column: [col], query: '^((?!' + val + ').)*$'});
          break;
        case 'startswith':
          this.filterLike.push({column: [col], query: '^' + val});
          break;
        case 'endswith':
          this.filterLike.push({column: [col], query: val + '$'});
          break;
        case '=':
          if (isValDateTime) {
            this.filterLike.push({column: [col], query: '^' + Utils.formatDateTime(new Date(val), 'Y-m-d')});
          } else {
            this.filterEqual.push({column: [col], value: val});
          }
          break;
        case '<>':
          if (isValDateTime) {
            this.filterLike.push({column: [col], query: '^((?!' + Utils.formatDateTime(new Date(val), 'Y-m-d') + ').)*$'});
          } else {
            this.filterExclude.push({column: [col], value: val});
          }
          break;
        case '<':
          if (isValDateTime) {
            const date = new Date(val);
            date.setDate(date.getDate());
            this.filterLessThen.push({column: col, valueTo: Utils.formatDateTime(date, 'Y-m-d')});
          } else {
            this.filterLessThen.push({column: col, valueTo: (parseInt(val, 10) - 1).toString()});
          }
          break;
        case '>':
          if (isValDateTime) {
            const date = new Date(val);
            date.setDate(date.getDate() + 1);
            this.filterGreaterThen.push({column: col, valueFrom: Utils.formatDateTime(date, 'Y-m-d')});
          } else {
            this.filterGreaterThen.push({column: col, valueFrom: (parseInt(val, 10) + 1).toString()});
          }
          break;
        case '<=':
          if (isValDateTime) {
            const date = new Date(val);
            date.setDate(date.getDate() + 1);
            this.filterLessThen.push({column: col, valueTo: Utils.formatDateTime(date, 'Y-m-d')});
          } else {
            this.filterLessThen.push({column: col, valueTo: val});
          }
          break;
        case '>=':
          this.filterGreaterThen.push({column: col, valueFrom: val});
          break;
        case 'isnotnull':
        case 'isnull':
        case 'isempty':
        case 'isnotempty':
          // TODO: Implementovat isnull
          alert('Testovací server nemá implementovánou možnost \'is null\', \'is not null\', \'is empty\', \'is not empty\'. Prozatimní data nemusí odpovídat zadanému filtru');
          break;
      }
    }
  }

  private getSortedArray(toSort: SortMeta[]): string[] {
    const sortArray: string[] = [];
    if (toSort) {
      toSort.forEach(sort => {
        sortArray.push(sort.field);
      });
    }
    return sortArray;
  }
  private getOrderedArray(toOrder: SortMeta[]): string[] {
    const orderArray: string[] = [];
    if (toOrder) {
      toOrder.forEach(sort => {
        orderArray.push(sort.order > 0 ? 'asc' : 'desc');
      });
    }
    return orderArray;
  }
  private resetFilters() {
    // @ts-ignore
    this.filterExclude = []; this.filterEqual = []; this.filterLike = []; this.filterGreaterThen = []; this.filterLessThen = [];
  }

}
