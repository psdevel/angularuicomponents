import {Routes} from '@angular/router';
import {PrimengComponent} from './primeng.component';

export const primengRoutes: Routes = [
  {
    path: '',
    component: PrimengComponent,
  }
];
