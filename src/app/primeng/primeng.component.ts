import {Component, Inject, OnInit} from '@angular/core';
import {Utils} from '../shared/utils/utils';
import {DOCUMENT} from '@angular/common';
import {EmployeeDto} from '../shared/dtos/employee.dto';
import {MockDataService} from '../shared/services/mock-data.service';
import {HttpParams} from '@angular/common/http';
import {EmployeeColumnsDefinition} from '../shared/constants/employee.columns-definition';
import {PAGE_SIZE} from '../shared/constants/app.constant';
import {LazyLoadEvent} from 'primeng/api';
import {PrimengService} from './primeng.service';
import {Table} from 'primeng/table';

@Component({
  selector: 'app-primeng',
  templateUrl: './primeng.component.html',
  styleUrls: ['./primeng.component.scss']
})
export class PrimengComponent implements OnInit {

  public employees: EmployeeDto[];
  public clonedEmployees: {[s: string]: EmployeeDto} = {};
  public selectedEmployees: EmployeeDto[];
  public state: LazyLoadEvent = {
    rows: PAGE_SIZE,
    first: 0
  };
  public totalRecords: number;
  public loading: boolean = true;
  public isAdding: boolean = false;

  public columnsSettings: PrimengColumnSetting[] = EmployeeColumnsDefinition.map(column => {
    return {
      field: column.field,
      header: column.title,
      fieldType: column.dataType,
      visible: column.visible,
    } as PrimengColumnSetting;
  });

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private primengService: PrimengService,
    private mockService: MockDataService,
  ) {
    Utils.lazyLoadStyles(['prime-icons.css', 'prime-theme.css', 'prime-style.css'], this.document);
  }

  ngOnInit() {
    this.loadData();
  }

  loadEmployeesLazy(event: LazyLoadEvent) {
    this.state = event;
    this.loadData();
  }

  fieldType2InputType(type: string): string {
    return Utils.filedType2InputType(type);
  }

  onRowEditInit(employee: EmployeeDto) {
    if (employee.id) {
      this.clonedEmployees[employee.id] = {...employee};
    } else {
      this.clonedEmployees[this.totalRecords + 1] = {...new EmployeeDto()};
    }
  }

  onRowEditSave(employee: EmployeeDto) {
    let service;
    if (employee.id) {
      service = this.mockService.putMockEmployees(employee.id, employee);
    } else {
      service = this.mockService.postMockEmployees(employee);
    }
    service.subscribe(() => {
      delete this.clonedEmployees[employee.id];
      this.loadData();
      this.isAdding = false;
    });
  }

  onRowDelete(employee: EmployeeDto) {
    this.mockService.deleteMockEmployees(employee.id).subscribe(() => {
      delete this.clonedEmployees[employee.id];
      this.loadData();
    });
  }

  onRowEditCancel(employee: EmployeeDto, index: number, table?: Table) {
    if (employee && employee.id) {
      this.employees[index] = this.clonedEmployees[employee.id];
      delete this.clonedEmployees[employee.id];
    } else {
      table.value.shift();
      delete this.clonedEmployees[this.totalRecords + 1];
    }
    this.isAdding = false;
  }

  newRow() {
    return new EmployeeDto();
  }

  private loadData(): void {
    const params: HttpParams = this.primengService.prepareParams(this.state);
    this.loading = true;
    this.mockService.getMockEmployees(params).subscribe((response) => {
      this.employees = response.body as unknown as EmployeeDto[];
      this.totalRecords = parseInt(response.headers.get('x-total-count'), 10);
      this.loading = false;
    });
  }
}

interface PrimengColumnSetting {
  field: string;
  header: string;
  fieldType: string;
  visible: boolean;
}
