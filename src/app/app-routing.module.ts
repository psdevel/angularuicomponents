import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dev-extreme',
    pathMatch: 'full'
  },
  {
    path: 'dev-extreme',
    loadChildren: () => import('./dev-extreme/dev-extreme.module').then(m => m.DevExtremeModule),
  },
  {
    path: 'kendo-ui',
    loadChildren: () => import('./kendo-ui/kendo-ui.module').then(m => m.KendoUiModule),
  },
  {
    path: 'primeng',
    loadChildren: () => import('./primeng/primeng.module').then(m => m.PrimengModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
