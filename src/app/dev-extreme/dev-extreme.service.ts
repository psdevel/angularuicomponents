import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {LoadOptions} from 'devextreme/data/load_options';
import {Utils} from '../shared/utils/utils';
import {isArray, isDate, isString} from 'util';

@Injectable({
  providedIn: 'root'
})
export class DevExtremeService {

  private filterLike: [{ column: string[], query: string }];
  private filterEqual: [{column: string[], value: string}];
  private filterExclude: [{ column: string[], value: string }];
  private filterGreaterThen: [{ column: string, valueFrom: string }];
  private filterLessThen: [{ column: string, valueTo: string }];

  constructor() {}

  prepareParams(params: LoadOptions): HttpParams {
    this.resetFilters();

    // TODO: implement or, not or, not and
    if (params.filter && isString(params.filter[1]) && params.filter[1] === 'or') {
      if (isDate(params.filter['filterValue'])) {
        // Handle 'not Equal' condition in Date
        const filteredDateValue: Date = params.filter['filterValue'] as Date;
        params.filter = [params.filter[0][0] as string, 'notcontains', Utils.formatDateTime(filteredDateValue, 'Y-m-d')];
        this.parseFilter(params.filter);
      } else {
        // Handle 'not Equal' condition in other cases
        alert('Testovací server nemá implementovánou možnost \'OR\'. Prozatimní data nemusí odpovídat zadanému filtru');
        return;
      }
    } else if (params.filter && isString(params.filter[0]) && params.filter[0] === '!') {
      // Handle group 'not' condition
      alert('Testovací server nemá implementovánou možnost \'negace\'. Prozatimní data nemusí odpovídat zadanému filtru');
      return;
    } else {
      params.filter && isArray(params.filter[0]) ? this.parseMultipleFilter(params.filter) : this.parseFilter(params.filter);
    }

    return Utils.prepareMockParams({
      pageSize: params.take,
      page: (params.skip / params.take) + 1,
      sort: this.getSortedArray(params.sort),
      order: this.getOrderedArray(params.sort),
      filtersLike: this.filterLike,
      filters: this.filterEqual,
      filtersExclude: this.filterExclude,
      filtersGraterThen: this.filterGreaterThen,
      filtersLessThen: this.filterLessThen,
    });
  }

  private getSortedArray(toSort: {selector: string, desc: boolean}[]): string[] {
    const sortArray: string[] = [];
    if (toSort) {
      toSort.forEach(sort => {
        sortArray.push(sort.selector);
      });
    }
    return sortArray;
  }
  private getOrderedArray(toOrder: {selector: string, desc: boolean}[]): string[] {
    const orderArray: string[] = [];
    if (toOrder) {
      toOrder.forEach(sort => {
        orderArray.push(sort.desc ? 'desc' : 'asc');
      });
    }
    return orderArray;
  }
  private parseMultipleFilter(filters: string[]|string[][]) {
    filters.forEach(filter => {
      if (isArray(filter)) {
        this.parseMultipleFilter(filter);
      }
      this.parseFilter(filter);
    });
  }
  private parseFilter(filter: string|string[]) {
    if (filter && filter.length >= 3) {
      const operator: string = filter[1];
      const col: string = filter[0];
      let val: string = filter[2]; let isValDateTime = false;

      if (isDate(val)) {
        val = Utils.formatDateTime(val as unknown as Date, 'Y-m-dTH:i:s.vZ');
        isValDateTime = true;
      }

      switch (operator) {
        case 'contains':
          this.filterLike.push({column: [col], query: val});
          break;
        case 'notcontains':
          this.filterLike.push({column: [col], query: '^((?!' + val + ').)*$'});
          break;
        case 'startswith':
          this.filterLike.push({column: [col], query: '^' + val});
          break;
        case 'endswith':
          this.filterLike.push({column: [col], query: val + '$'});
          break;
        case '=':
          this.filterEqual.push({column: [col], value: val});
          break;
        case '<>':
          this.filterExclude.push({column: [col], value: val});
          break;
        case '<':
          if (isValDateTime) {
            this.filterLessThen.push({column: col, valueTo: val});
          } else {
            this.filterLessThen.push({column: col, valueTo: (parseInt(val, 10) - 1).toString()});
          }
          break;
        case '>':
          if (isValDateTime) {
            this.filterGreaterThen.push({column: col, valueFrom: val});
          } else {
            this.filterGreaterThen.push({column: col, valueFrom: (parseInt(val, 10) + 1).toString()});
          }
          break;
        case '<=':
          this.filterLessThen.push({column: col, valueTo: val});
          break;
        case '>=':
          this.filterGreaterThen.push({column: col, valueFrom: val});
          break;
      }
    }
  }
  private resetFilters() {
    // @ts-ignore
    this.filterExclude = []; this.filterEqual = []; this.filterLike = []; this.filterGreaterThen = []; this.filterLessThen = [];
  }
}
