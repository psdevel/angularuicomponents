import {Routes} from '@angular/router';
import {DevExtremeComponent} from './dev-extreme.component';

export const devExtremeRoutes: Routes = [
  {
    path: '',
    component: DevExtremeComponent,
  }
];
