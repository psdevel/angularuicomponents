import {Component, Inject, OnInit} from '@angular/core';
import {MockDataService} from '../shared/services/mock-data.service';
import {EmployeeDto} from '../shared/dtos/employee.dto';
import DataSource from 'devextreme/data/data_source';
import {HttpResponse} from '@angular/common/http';
import {DevExtremeService} from './dev-extreme.service';
import {PAGE_SIZE} from '../shared/constants/app.constant';
import {EmployeeColumnsDefinition} from '../shared/constants/employee.columns-definition';
import DevExpress from 'devextreme/bundles/dx.all';
import GridBaseColumn = DevExpress.ui.GridBaseColumn;
import {Utils} from '../shared/utils/utils';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'app-dev-extreme',
  templateUrl: './dev-extreme.component.html',
  styleUrls: ['./dev-extreme.component.scss']
})
export class DevExtremeComponent implements OnInit {

  constructor(
    private mockService: MockDataService,
    private devExtremeService: DevExtremeService,
    @Inject(DOCUMENT) private document: Document,
  ) {
    Utils.lazyLoadStyles(['devextreme-ui-style.css', 'devextreme-ui-theme.css'], this.document);
  }

  public gridDataSource: any = {};
  public pageSize: number = PAGE_SIZE;
  public pageIndex: number = 0;

  filterValue: Array<any> = [];

  public columns: GridBaseColumn[] = EmployeeColumnsDefinition.map(value => {
    return {
      dataType: value.dataType,
      dataField: value.field,
      caption: value.title,
      visible: value.visible,
    } as GridBaseColumn;
  });


  applyFilter(filterExpression) {
    this.filterValue = filterExpression;
  }

  ngOnInit() {
    this.gridDataSource = new DataSource({
      key: 'id',
      load: (loadOptions) => {
        const params = this.devExtremeService.prepareParams(loadOptions);
        return this.mockService.getMockEmployees(params)
          .toPromise()
          .then((result: HttpResponse<EmployeeDto>) => {
            return {
              data: result.body,
              totalCount: result.headers.get('x-total-count'),
            };
          });
      },
      update: (id, values) => {
        const employeeDto: EmployeeDto = this.gridDataSource._items.find(employee => employee.id === id);
        const valuesKeys: string[] = Object.keys(values);
        valuesKeys.forEach(key => {
          employeeDto[key] = values[key];
        });
        return this.mockService.putMockEmployees(id, employeeDto)
          .toPromise()
          .then((result: HttpResponse<EmployeeDto>) => {
            return {
              data: result.body
            };
          });
      },
      insert: (values) => {
        return this.mockService.postMockEmployees(values as EmployeeDto)
          .toPromise()
          .then((result: HttpResponse<EmployeeDto>) => {
            return {
              data: result.body
            };
          });
      },
      remove: (key) => {
        return this.mockService.deleteMockEmployees(key)
          .toPromise()
          .then((result: HttpResponse<EmployeeDto>) => {
            return {
              data: result.body
            };
          });
      }
    });
  }
}
