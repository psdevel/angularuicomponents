import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevExtremeComponent } from './dev-extreme.component';
import {RouterModule} from '@angular/router';
import {devExtremeRoutes} from './dev-extreme.route';
import {SharedModule} from '../shared/shared.module';
import {DxDataGridModule} from 'devextreme-angular';
import {DevExtremeService} from './dev-extreme.service';

@NgModule({
  declarations: [
    DevExtremeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(devExtremeRoutes),

    DxDataGridModule,
  ],
  providers: [
    DevExtremeService,
  ]
})
export class DevExtremeModule { }
